Name: Jon Raphael Apostol
Student ID#: 32302252

I, Jon Raphael Apostol, hereby certify that the files I submitted represent my
own work, that I did not copy any code from any other person or source, and that I did
not share my code with any other students.

I do not own the music included in the raw files. The music is owned by Konami Digital Computer
Entertainment, Inc. The song is named "Free Flyer" from a retro shoot-'em-up arcade game, Gradius.
