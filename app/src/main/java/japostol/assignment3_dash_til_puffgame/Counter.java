package japostol.assignment3_dash_til_puffgame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Jon Apostol on 4/28/2015.
 */
public class Counter {
    private int width;
    private int height;

    private int x;
    private int displayScore;
    private Paint paint;

    public Counter(DashTillPuffSurfaceView view){
        x = 0;
        displayScore = 0;

        width = view.getWidth();
        height = view.getHeight();

        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(height/12);

    }

    public void draw(Canvas c){
        x++;
        if(x%50 == 49)
            displayScore++;
        c.drawText("Score: " + displayScore, 5*width/6, 11*height/12, paint);
    }
}
