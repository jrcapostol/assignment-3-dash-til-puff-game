package japostol.assignment3_dash_til_puffgame;

import android.graphics.Canvas;

/**
 * Created by Jon Apostol on 4/20/2015.
 */
public interface TimeConscious {
    public void tick(Canvas canvas);
}
