package japostol.assignment3_dash_til_puffgame;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.media.MediaPlayer;


public class MainActivity extends ActionBarActivity {

    MediaPlayer playTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DashTillPuffSurfaceView(getBaseContext()));
        playTheme = MediaPlayer.create(MainActivity.this, R.raw.theme);
        playTheme.setLooping(true);
        playTheme.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        playTheme.pause();

    }

    @Override
    protected void onResume(){
        super.onResume();
        playTheme.start();
    }



}
