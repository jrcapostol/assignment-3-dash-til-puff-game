package japostol.assignment3_dash_til_puffgame;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Jon Apostol on 4/27/2015.
 */
public class CosmicObject {
    int x;
    int y;

    private Bitmap pic;
    private Paint paint;
    private Rect dst;
    private int image;
    private int width;
    private int height;

    public CosmicObject(int image, DashTillPuffSurfaceView view){
        width = view.getWidth();
        height = view.getHeight();
        this.image = image;
        paint = new Paint();
        paint.setAlpha(255);
        pic = BitmapFactory.decodeResource(view.getResources(), image);
    }

    public void isHit(Ship s){
        if( (s.x + s.maxX/16) >= (x - width/32 - 32*width / 144) && (s.x - s.maxX/16) <= (x + width/32 - 32*width / 144) && (s.y-s.maxY/9) <= (y + 32) && (s.y+s.maxY/9) >= (y - 32))
            s.hit(true);
    }

    public void draw(Canvas c){
        dst = new Rect(x - width/32, y - height/18, x + width/32, y + height/18);
        c.drawBitmap(pic, null, dst, paint);
    }

    protected int returnImage(){
        return image;
    }
}
