package japostol.assignment3_dash_til_puffgame;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;

/**
 * Created by Jon Apostol on 4/21/2015.
 */
public class Ship {

    MediaPlayer crashSound;

    //POSITION
    int x = 0;
    int y = 0;
    int maxY;
    int maxX;
    boolean isFlying = false;

    //VIEW
    DashTillPuffSurfaceView view;

    //DETERMINES IF HIT
    boolean crashed = false;
    boolean isDisintegrated = false;
    boolean restart = false;

    //APPEARANCE
    int alpha = 255;
    Paint paint;
    Bitmap pic;
    Rect dst;

    //CONSTRUCTOR
    public Ship(int maxX, int maxY, DashTillPuffSurfaceView v){

        x = maxX/12;
        y = 0;

        view = v;

        //WHERE GETHEIGHT IS ASSIGNED
        this.maxX = maxX;
        this.maxY = maxY;

        //MAKES RECTANGLE FOR BITMAP
        paint = new Paint();
        paint.setAlpha(alpha);
        pic= BitmapFactory.decodeResource(view.getResources(), R.drawable.spaceship);

    }

    //METHODS

    public void doesFly(boolean x){
        isFlying = x;
    }

    public void flies(){
        if(y - 64 > 0 && isFlying && !crashed)
            y -= 16;
        else if (y + 64 < maxY && !isFlying && !crashed)
            y += 16;
    }

    public void hit(Boolean x){
        crashed = x;
        isDisintegrated = x;
        crashSound = MediaPlayer.create(view.getContext(), R.raw.crashed);
        crashSound.start();
    }

    public boolean isCrashed(){
        return crashed;
    }

    public void disintegrates(){
        //DISAPPEARS WHEN HIT

        if(isDisintegrated){
            alpha -= 51;
            paint.setAlpha(alpha);

            if(alpha <= 0){
                isDisintegrated = false;
                crashed = false;

                //SETS A BOOLEAN VALUE TO RESTART THE GAME WHEN FULLY DISINTEGRATED
                restart = true;
            }
        }
    }




    public boolean restartGame(){
        return restart;
    }

    public void draw(Canvas c){
    //DRAWS SHIP TO CANVAS
        dst = new Rect(4*x - maxX/16, y - maxY/9, 4*x + maxX/16, y + maxY/9);
        c.drawBitmap(pic, null, dst, paint);
    }

}
