package japostol.assignment3_dash_til_puffgame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Jon Apostol on 4/22/2015.
 */
public class Trajectory implements TimeConscious {
    private ArrayList<Point2D> points = new ArrayList<Point2D>();
    private int width;
    private int height;
    protected int rand;
    protected int rand2;
    protected Random randomGen;

    public Trajectory(DashTillPuffSurfaceView view){
        width = view.getWidth();
        height = view.getHeight();

        randomGen = new Random();

        rand = randomGen.nextInt(3);
        rand2 = randomGen.nextInt(3);

        //INITIALIZES POINTS OF TRAJECTORY
        points.add(new Point2D(0, height/2));
        points.add(new Point2D(width/3, height/7));
        points.add(new Point2D(2*width/3, height/7));
        points.add(new Point2D(width, height /2));
        points.add(new Point2D(4*width/3, randPos(rand)));
        points.add(new Point2D(5*width/3, randPos(rand)));
        points.add(new Point2D(2*width, height/2));
        points.add(new Point2D(7*width/3, randPos(rand2)));
        points.add(new Point2D(8*width/3, randPos(rand2)));
        points.add(new Point2D(3*width, height /2));
        points.add(new Point2D(10*width/3, 6*height/7));
        points.add(new Point2D(11*width/3, 6*height/7));
        points.add(new Point2D(4*width, height/2));
        points.add(new Point2D(13*width/3, height/7));
        points.add(new Point2D(14*width/3, height/7));
        points.add(new Point2D(5*width, height/2));

    }

    @Override
    public void tick(Canvas canvas){
        for(int i = 0; i < points.size(); i++){
            //MOVES TRAJECTORY
            points.get(i).x -= width/144;
            if(points.get(i).x < -4*width){
                reset();
                break;
            }
        }

        draw(canvas);

    }

    private void draw(Canvas c){
        Point2D p = points.get(0);

        for(int i = 0; i < points.size(); i++){
            if(points.get(i).x == 0) {
                p = points.get(i);
                break;
            }
        }

        Path path = new Path();
        path.moveTo(p.x, p.y);
        for(int i = 1; i < points.size(); ++i){
            path.lineTo(points.get(i).x, points.get(i).y);
        }

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAlpha(255);
        paint.setStyle(Paint.Style.STROKE);
        paint.setPathEffect(new DashPathEffect(new float[] {10, 20}, 0));

        c.drawPath(path, paint);
    }

    private void reset(){
        rand = randomGen.nextInt(3);
        rand2 = randomGen.nextInt(3);

        points.get(0).x = 0;
        points.get(1).x = width/3;
        points.get(2).x = 2*width/3;
        points.get(3).x = width;
        points.get(4).x = 4*width/3;
        points.get(4).y = randPos(rand);
        points.get(5).x = 5*width/3;
        points.get(5).y = randPos(rand);
        points.get(6).x = 2*width;
        points.get(7).x = 7*width/3;
        points.get(7).y = randPos(rand2);
        points.get(8).x = 8*width/3;
        points.get(8).y = randPos(rand2);
        points.get(9).x = 3*width;
        points.get(10).x = 10*width/3;
        points.get(11).x = 11*width/3;
        points.get(12).x = 4*width;
        points.get(13).x = 13*width/3;
        points.get(14).x = 14*width/3;
        points.get(15).x = 5*width;

    }

    private int randPos(int x){
        if(x%3 == 0){
            return height/7;
        }
        else if (x%3 == 1){
            return height/2;
        }

        return 6*height/7;
    }
}