package japostol.assignment3_dash_til_puffgame;

/**
 * Created by Jon Apostol on 4/22/2015.
 */
public class Point2D {
    //MAPS THE POINTS OF THE TRAJECTORY
    int x;
    int y;

    //SETS THE X AND Y VALUES
    public Point2D(int x, int y){
        this.x = x;
        this.y = y;
    }
}
