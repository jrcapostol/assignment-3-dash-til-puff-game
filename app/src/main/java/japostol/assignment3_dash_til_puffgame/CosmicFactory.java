package japostol.assignment3_dash_til_puffgame;

import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Jon Apostol on 4/22/2015.
 */
public class CosmicFactory implements TimeConscious {
    private int width;
    private int height;
    private DashTillPuffSurfaceView view;

    ArrayList<CosmicObject> cosmos = new ArrayList<CosmicObject>();



    public CosmicFactory(DashTillPuffSurfaceView view){
        width = view.getWidth();
        height = view.getHeight();
        this.view = view;

        Random random = new Random();
        int rand = 0;

        //GENERATES RANDOM OBJECTS
        //NOTE: MADE INTO 50 TO AVOID STACK OVERLOAD AND CRASHING
        for(int i = 0; i < 50; i++){

            if(i < 40) {
                if (i % 10 == 0)
                    rand = random.nextInt(10);
                cosmos.add(new CosmicObject(generateObject(rand), view));
            }
            else{
                cosmos.add(new CosmicObject(cosmos.get(i%20).returnImage(), view));
            }
        }

        int cons = 0;

        for (int i = 0; i < cosmos.size(); i++) {
            if((i % 10) == 0 && i != 0)
                cons++;

            if(cons% 4 == 0) {
                //INITIALIZATION
                if (i % 10 == 0) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 1) {
                    cosmos.get(i).x = 13 * width / 30 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 2) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 3) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 4) {
                    cosmos.get(i).x = 2 * width / 3 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 5) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 6) {
                    cosmos.get(i).x = 13 * width / 30 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 7) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 8) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 9) {
                    cosmos.get(i).x = 2 * width / 3 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                }
            }


            //trajectory path 2
            else if(cons%4 == 1) {
                //HIGH
                if(view.trajectory.rand == 0){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height/2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height /2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 11 * height/12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    }

                }
                //MIDDLE
                else if(view.trajectory.rand == 1){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    }
                }
                //LOW
                else{
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    }
                }
            }
            //trajectory part 3
            else if(cons%4 == 2) {
                //HIGH
                if(view.trajectory.rand2 == 0){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height/2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height /2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 11 * height/12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    }

                }
                //MIDDLE
                else if(view.trajectory.rand2 == 1){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    }
                }
                //LOW
                else{
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    }
                }
            }
            else if (cons % 4 == 3){
                if (i % 10 == 0) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 1) {
                    cosmos.get(i).x =  13*width / 30 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 2) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 3) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 4) {
                    cosmos.get(i).x = 2 * width / 3 +  cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 5) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 6) {
                    cosmos.get(i).x =  13*width / 30 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 7) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 8) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 9) {
                    cosmos.get(i).x = 2 *width / 3 + cons * width;
                    cosmos.get(i).y = height / 7;
                }
            }
        }
    }

    @Override
    public void tick(Canvas canvas){

        for (int i = 0; i < cosmos.size(); i++) {
            if (cosmos.get(i).x < -11*width/3) {
                reset();
                break;
            }
            cosmos.get(i).x -= width / 144;
        }

        for(int i = 0; i < cosmos.size(); i++) {
            //MOVES TRAJECTORY
            cosmos.get(i).isHit(view.ship);
            cosmos.get(i).draw(canvas);
        }


    }

    private int generateObject(int x){
        switch(x){
            //BLACK HOLE
            case 0:
                return R.drawable.blackhole;
            //BLUE PLANET
            case 1:
                return R.drawable.blueplanet;
            // GLOSSY PLANET
            case 2:
                return R.drawable.glossyplanet;
            //GOLDEN STAR
            case 3:
                return R.drawable.goldenstar;
            //NEUTRON STAR
            case 4:
                return R.drawable.neutronstar;
            //PUFF CLOUD
            case 5:
                return R.drawable.puffcloud;
            //PUFF EARTH
            case 6:
                return R.drawable.puffearth;
            //PUFF STAR 1
            case 7:
                return R.drawable.puffstar1;
            //PUFF STAR 2
            case 8:
                return R.drawable.puffstar2;
            //SHINY STAR
            case 9:
                return R.drawable.shinystar;

        }
        //GIVES RANDOM IF NOT MATCHED
        return  R.drawable.shinystar;
    }

    private void reset() {
        int cons = 0;

        for (int i = 0; i < cosmos.size(); i++) {
            if((i % 10) == 0 && i != 0)
                cons++;

            if(cons% 4 == 0) {
                //INITIALIZATION
                if (i % 10 == 0) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 1) {
                    cosmos.get(i).x = 13 * width / 30 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 2) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 3) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 4) {
                    cosmos.get(i).x = 2 * width / 3 + cons * width;
                    cosmos.get(i).y = 7 * height / 8;
                } else if (i % 10 == 5) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 6) {
                    cosmos.get(i).x = 13 * width / 30 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 7) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 8) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                } else if (i % 10 == 9) {
                    cosmos.get(i).x = 2 * width / 3 + cons * width;
                    cosmos.get(i).y = 5 * height / 8;
                }
            }


            //trajectory path 2
            else if(cons%4 == 1) {
                //HIGH
                if(view.trajectory.rand == 0){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height/2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height /2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 11 * height/12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    }

                }
                //MIDDLE
                else if(view.trajectory.rand == 1){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    }
                }
                //LOW
                else{
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    }
                }
            }
            //trajectory part 3
            else if(cons%4 == 2) {
                //HIGH
                if(view.trajectory.rand2 == 0){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height/2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height /2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 11 * height/12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 11*height / 12;
                    }

                }
                //MIDDLE
                else if(view.trajectory.rand2 == 1){
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 7;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = 6*height / 7;
                    }
                }
                //LOW
                else{
                    if (i % 10 == 0) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 1) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 2) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 3) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 4) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 2;
                    } else if (i % 10 == 5) {
                        cosmos.get(i).x = width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 6) {
                        cosmos.get(i).x =  13*width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 7) {
                        cosmos.get(i).x = width / 2 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 8) {
                        cosmos.get(i).x = 17 * width / 30 + cons * width;
                        cosmos.get(i).y = height / 12;
                    } else if (i % 10 == 9) {
                        cosmos.get(i).x = 2 * width / 3 + cons * width;
                        cosmos.get(i).y = height / 12;
                    }
                }
            }
            else if (cons % 4 == 3){
                if (i % 10 == 0) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 1) {
                    cosmos.get(i).x =  13*width / 30 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 2) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 3) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 4) {
                    cosmos.get(i).x = 2 * width / 3 + cons * width;
                    cosmos.get(i).y = height / 2;
                } else if (i % 10 == 5) {
                    cosmos.get(i).x = width / 3 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 6) {
                    cosmos.get(i).x =  13*width / 30 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 7) {
                    cosmos.get(i).x = width / 2 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 8) {
                    cosmos.get(i).x = 17 * width / 30 + cons * width;
                    cosmos.get(i).y = height / 7;
                } else if (i % 10 == 9) {
                    cosmos.get(i).x = 2*width / 3 + cons * width;
                    cosmos.get(i).y = height / 7;
                }
            }
        }

    }
}
