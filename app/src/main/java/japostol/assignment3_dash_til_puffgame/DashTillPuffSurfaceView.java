package japostol.assignment3_dash_til_puffgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Jon Apostol on 4/20/2015.
 */
public class DashTillPuffSurfaceView extends SurfaceView implements SurfaceHolder.Callback, TimeConscious {

    Thread renderThread;
    int alpha = 255;
    Bitmap bitmap;
    Ship ship;
    Trajectory trajectory;
    CosmicFactory cosmos;

    //Score
    Counter score;

    public DashTillPuffSurfaceView ( Context context ) {
        super(context) ;
        getHolder().addCallback(this) ;
    }

    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        renderThread = new DashTillPuffRenderThread ( this ) ;
        renderThread.start() ;
// Create the sliding background , cosmic factory , trajectory
// and the space ship
        trajectory = new Trajectory(this);
        ship = new Ship(getWidth(), getHeight(), this);
        cosmos = new CosmicFactory(this);
        score = new Counter(this);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

    }

    @Override
    public boolean onTouchEvent ( MotionEvent e ) {
        switch ( e . getAction () ) {
            case MotionEvent.ACTION_DOWN:
            // Thrust the space ship up .
                if(!ship.isCrashed())
                    ship.doesFly(true);
                break;
            case MotionEvent.ACTION_UP :
            // Let space ship fall freely .
                if(!ship.isCrashed())
                    ship.doesFly(false);
                break;
        }
        return true ;
    }
    @Override
    public void onDraw ( Canvas c ) {
    // Draw everything ( restricted to the displayed rectangle ) .
        super.onDraw(c);


        //BACKGROUND
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.background, options);
        Paint paint = new Paint();
        paint.setAlpha(255);
        Rect dst = new Rect(0, 0, getWidth(), getHeight());
        c.drawBitmap(bitmap, null, dst, paint);

        //RESTARTS GAME
        if(ship.restart){
            trajectory = new Trajectory(this);
            ship = new Ship(getWidth(), getHeight(), this);
            cosmos = new CosmicFactory(this);
            score = new Counter(this);
        }

        if(ship.isDisintegrated) {
            ship.disintegrates();
        }

        trajectory.tick(c);
        cosmos.tick(c);
        ship.draw(c);
        score.draw(c);

    }
    @Override
    public void tick(Canvas c) {
    // Tick background , space ship , cosmic factory , and trajectory .
    // Draw everything ( restricted to the displayed rectangle ) .


        //SHIP'S POSITION
        ship.flies();

        //DRAWS EVERYTHING
        onDraw(c);
    }


}
